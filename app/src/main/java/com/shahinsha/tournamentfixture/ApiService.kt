package com.shahinsha.tournamentfixture

import com.shahinsha.tournamentfixture.pojo.*
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiService {

    @POST("users")
    fun registerUser(
        @Body registerReq: RegisterReq): Observable<UserRes>

    @POST("/users/login")
    fun loginUser(
        @Body LoginReq: LoginReq) : Observable<LoginRes>

    @GET("/users/me")
    fun getProfile(@Header("Authorization") authkey: String?): Observable<User>

    @POST("/users/tournaments")
    fun getTournaments(
        @Body Gettournaments: GetTournamentReq, @Header("Authorization") authkey: String?): Observable<GetTournamentRes>

    companion object Factory {
        fun create(URL: String): ApiService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(URL)
                .build()
            return retrofit.create(ApiService::class.java);
        }
    }
}