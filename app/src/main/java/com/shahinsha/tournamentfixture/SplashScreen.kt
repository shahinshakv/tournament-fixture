package com.shahinsha.tournamentfixture

import android.content.Intent
import com.shahinsha.tournamentfixture.SharedPreference.SharedPreference
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log

class SplashScreen : AppCompatActivity() {

    private var handler: Handler? = null
    private var accessToken: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
         var SharedPreference = SharedPreference()
        accessToken = SharedPreference.getValue(this, "AccessToken")

        handler = Handler()

        val runnable = Runnable {

            Log.i("SplashScreen", "onCreate (line 32): ")
            var i: Intent

            if (accessToken==null) {
                i = Intent(this@SplashScreen, MainActivity::class.java)

            }
            else{
                i = Intent(this@SplashScreen, Map::class.java)
            }
            startActivity(i)
            // close this activity
            finish()
        }
        handler?.postDelayed(runnable, 2000)
    }
    }

