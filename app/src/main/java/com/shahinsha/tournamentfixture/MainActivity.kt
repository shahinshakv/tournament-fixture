package com.shahinsha.tournamentfixture

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import com.google.gson.Gson
import com.shahinsha.tournamentfixture.SharedPreference.SharedPreference
import com.shahinsha.tournamentfixture.pojo.DefaultError
import com.shahinsha.tournamentfixture.pojo.LoginReq
import com.shahinsha.tournamentfixture.pojo.RegisterReq
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.adapter.rxjava2.HttpException
import java.io.IOException

class MainActivity : AppCompatActivity() {

    val TAG = MainActivity::class.java.simpleName

    val api = ApiService.create(WAZZA_URL)
    var userId: String? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var teamName = findViewById<EditText>(R.id.teamName)
        var leaderName = findViewById<EditText>(R.id.leaderName)
        var phone = findViewById<EditText>(R.id.phone)
        var email = findViewById<EditText>(R.id.email)
        var password = findViewById<EditText>(R.id.password)
        var signup =findViewById<Button>(R.id.signup)
        var create = findViewById<TextView>(R.id.create)
        var emaillogin = findViewById<EditText>(R.id.loginemail)
        var passlogin = findViewById<EditText>(R.id.loginpass)
        var login = findViewById<Button>(R.id.login)
        var already = findViewById<TextView>(R.id.already)
        var uptext = findViewById<TextView>(R.id.txtup)
        var clubdp = findViewById<ImageView>(R.id.upimg)
        var fixture = findViewById<ImageView>(R.id.fixturewind)


        create.setOnClickListener{
            emaillogin.visibility = View.GONE
            passlogin.visibility = View.GONE
            login.visibility = View.GONE
            create.visibility= View.GONE
            fixture.visibility = View.GONE
            teamName.visibility = View.VISIBLE
            leaderName.visibility = View.VISIBLE
            phone.visibility = View.VISIBLE
            email.visibility = View.VISIBLE
            password.visibility = View.VISIBLE
            signup.visibility = View.VISIBLE
            already.visibility = View.VISIBLE
            uptext.visibility = View.VISIBLE
            clubdp.visibility = View.VISIBLE

        }

        already.setOnClickListener {
            emaillogin.visibility = View.VISIBLE
            passlogin.visibility = View.VISIBLE
            login.visibility = View.VISIBLE
            create.visibility= View.VISIBLE
            fixture.visibility = View.VISIBLE
            teamName.visibility = View.GONE
            leaderName.visibility = View.GONE
            phone.visibility = View.GONE
            email.visibility = View.GONE
            password.visibility = View.GONE
            signup.visibility = View.GONE
            already.visibility = View.GONE
            uptext.visibility = View.GONE
            clubdp.visibility = View.GONE
        }

        login.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            var SharedPreference = SharedPreference()

            val loginReq = LoginReq(emaillogin.text.toString(),passlogin.text.toString())
            api.loginUser(loginReq)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    dismissProgress(progressBar, this)
                    val handler = Handler()
                    handler.postDelayed({
                    }, 1000)
                    if (it.success==true) {

                        SharedPreference.save(this,"AccessToken",it.user.token)

                        Toast.makeText(
                            this,
                            "Login Successfully"+""+ it.user.leaderName,
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.d(TAG, "resultss" + it)

                       var i = Intent(this@MainActivity,Map::class.java)
                        startActivity(i)
                        finish()

                    }

                    Log.d(TAG, "Response" + it)
                },{dismissProgress(progressBar, this)
                    var error = parseError(it)
                    if (error != null) {
                        Toast.makeText(
                            this,
                            "Please check your email or password",
                            Toast.LENGTH_SHORT
                        ).show()
                        return@subscribe
                    }
                    it.printStackTrace()
                    Log.i("TAG", "onCreate (line 65): "+it)
                    Toast.makeText(this, "Could not connect to server.\nPlease check your Internet connection."+it, Toast.LENGTH_SHORT).show()
                })

        }




        signup.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            var SharedPreference = SharedPreference()

            val registerReq = RegisterReq(teamName.text.toString(),leaderName.text.toString(),
                phone.text.toString(),email.text.toString(),password.text.toString())
            api.registerUser(registerReq)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    dismissProgress(progressBar, this)
                    val handler = Handler()
                    handler.postDelayed({
                    }, 1000)
                    if (it.success==true) {
                        var l: Intent

                        l = Intent(this@MainActivity, Map::class.java)
                        startActivity(l)
                        finish()
                        SharedPreference.save(this,"AccessToken",it.User.token)

                        Toast.makeText(
                            this,
                            "Successfully created team.\n" + it.User.leaderName,
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.d(TAG, "result" + it)
                        var i = Intent(this@MainActivity,Profile::class.java)
                        startActivity(i)


                    }

                    Log.d(TAG, "Response" + it)
                },{dismissProgress(progressBar, this)
                    var error = parseError(it)
                    if (error != null) {
                        Toast.makeText(
                            this,
                            "Already linked these email or password",
                            Toast.LENGTH_SHORT
                        ).show()
                        return@subscribe
                    }
                    it.printStackTrace()
                    Log.i("TAG", "onCreate (line 65): "+it)
                    Toast.makeText(this, "Could not connect to server.\nPlease check your Internet connection."+it, Toast.LENGTH_SHORT).show()
                })

        }


    }
    fun dismissProgress(view: View, activity: Activity) {
        view.visibility = View.GONE
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
    private fun parseError(throwedError: Throwable): String? {
        var errorToReturn: String? = null
        if (throwedError is HttpException) {
            val error = throwedError
            val errorBody = error.response().errorBody()
            val gson = Gson()
            val adapter = gson.getAdapter<DefaultError>(DefaultError::class.java)
            try {
                val errorParser = adapter.fromJson(errorBody!!.string())
                errorToReturn = errorParser.error
                Log.d(TAG, "Error" + errorParser.error)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return errorToReturn
    }


}
