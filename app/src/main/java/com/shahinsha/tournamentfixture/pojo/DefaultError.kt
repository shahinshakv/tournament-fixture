package com.shahinsha.tournamentfixture.pojo

data class DefaultError(
    val error: String,
    val success: Boolean
)