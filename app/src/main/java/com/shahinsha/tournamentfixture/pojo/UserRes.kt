package com.shahinsha.tournamentfixture.pojo

import com.google.gson.annotations.SerializedName

data class UserRes (

    val message : String,
    val User : User,
    val success : Boolean
)

data class LoginRes (

    @SerializedName("User")
    val user : User,
    @SerializedName("token")
    val token : String,
    @SerializedName("success")
    val success : Boolean
)

data class User (



    @SerializedName("_coordinates")
    val _coordinates : List<Double>,
    @SerializedName("_id")
    val _id : String,
    @SerializedName("teamName")
    val teamName : String,
    @SerializedName("leaderName")
    val leaderName : String,
    @SerializedName("phone")
    val phone : String,
    @SerializedName("photo")
    val photo : String,
    @SerializedName("email")
    val email : String,
    @SerializedName("password")
    val password : String,
    @SerializedName("tournaments")
    val tournaments : List<Tournaments>,
    @SerializedName("createdAt")
    val createdAt : String,
    @SerializedName("updatedAt")
    val updatedAt : String,
    @SerializedName("__v")
    val __v : Int,
    @SerializedName("token")
    val token : String

)

data class Tournaments (

    @SerializedName("active")
    val active : Boolean,
    @SerializedName("_id")
    val _id : String,
    @SerializedName("tname")
    val tname : String,
    @SerializedName("tplayers")
    val tplayers : Int,
    @SerializedName("fee")
    val fee : Int,
    @SerializedName("date")
    val date : String,
    @SerializedName("description")
    val description : String,
    @SerializedName("createdAt")
    val createdAt : String,
    @SerializedName("updatedAt")
    val updatedAt : String
)

data class GetTournamentRes (

    @SerializedName("tournaments")
    val tournaments : List<Tournament>,
    @SerializedName("success")
    val success : Boolean
)

data class Tournament (

    @SerializedName("_id")
    val _id : String,
    @SerializedName("tournaments")
    val tournaments : List<Tournaments>,
    @SerializedName("name")
    val name : String,
    @SerializedName("photo")
    val photo : String,
    @SerializedName("phone")
    val phone : String,
    @SerializedName("location")
    val location : Locations,
    @SerializedName("createdAt")
    val createdAt : String,
    @SerializedName("updatedAt")
    val updatedAt : String
)
data class Locations (

    @SerializedName("type")
    val type : String,
    @SerializedName("coordinates")
    val coordinates : List<Double>
)
