package com.shahinsha.tournamentfixture.pojo

import com.google.gson.annotations.SerializedName


data class RegisterReq(

    val teamName: String,
    val leaderName: String,
    val phone: String,
    val email: String,
    val password: String
)

data class LoginReq(
    val email: String,
    val password: String
)

data class GetTournamentReq (

    @SerializedName("location")
    val location : Location,
    @SerializedName("radius")
    val radius : Double
)
data class Location (

    @SerializedName("latitude") val latitude : Double,
    @SerializedName("longitude") val longitude : Double
)