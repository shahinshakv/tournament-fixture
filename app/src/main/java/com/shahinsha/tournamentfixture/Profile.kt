package com.shahinsha.tournamentfixture

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.shahinsha.tournamentfixture.SharedPreference.SharedPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class Profile : AppCompatActivity() {

    val TAG = MainActivity::class.java.simpleName

    val api = ApiService.create(WAZZA_URL)
    var sharedPreference = SharedPreference()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile)

        var teamname = findViewById<TextView>(R.id.teamNamep)
        var leadername = findViewById<TextView>(R.id.leaderNamep)
        var email = findViewById<TextView>(R.id.emailp)
        var phone = findViewById<TextView>(R.id.phonep)



       var accessToken= sharedPreference.getValue(this,"AccessToken")

        api.getProfile(accessToken)
            .subscribeOn(Schedulers.newThread())

            .observeOn(AndroidSchedulers.mainThread())

            .subscribeOn(Schedulers.io())

            .subscribe({

                Log.i("Profile", "loadProfile (line 39): $it")

                    teamname.setText(it.teamName)
                    leadername.setText(it.leaderName)
                    email.setText(it.email)
                    phone.setText(it.phone)

            }, {
                Log.e("UserViewModel", "loadUsers (line 57): ", it)

            })

    }
}
