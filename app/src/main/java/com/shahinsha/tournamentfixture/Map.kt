package com.shahinsha.tournamentfixture


import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.solver.widgets.ConstraintWidget
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import com.shahinsha.tournamentfixture.SharedPreference.SharedPreference
import com.shahinsha.tournamentfixture.pojo.GetTournamentReq
import com.shahinsha.tournamentfixture.pojo.GetTournamentRes
import com.shahinsha.tournamentfixture.pojo.Tournament
import com.shahinsha.tournamentfixture.pojo.Tournaments
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.bottom_sheet.*
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.nav_header_home.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime;
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.ArrayList
import kotlin.properties.Delegates


class Map : AppCompatActivity(),NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    var permissionListener = object : PermissionListener {

        @SuppressLint("MissingPermission")
        override fun onPermissionGranted() {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->

                    if (location?.latitude != null) {

                        userLocation = LatLng(location.latitude, location.longitude)

                        Log.i("Map", "check location type" + userLocation)

                        moveCameratoUser()

                        getTournaments()

                        TournamentMap.isMyLocationEnabled = true




                        }



                }
        }

        override fun onPermissionDenied(deniedPermissions: ArrayList<String>?) {
            Log.i(TAG, "Permission was denied")

        }

    }
    val TAG = Map::class.java.simpleName

    val PERMISSION_ID = 42
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var longitude : String
    lateinit var latitude : String
    private lateinit var marker: Marker
    private lateinit var TournamentMap: GoogleMap
    private lateinit var userLocation: LatLng
    var sharedPreference = SharedPreference()
    private lateinit var selectedTournaments: Tournament
    lateinit var selectedTournamentId: String
    val api = ApiService.create(WAZZA_URL)
    private lateinit var Fixtures: List<Tournament>
    private var locationManager : LocationManager? = null
    private lateinit var detailsSheetBehavior: BottomSheetBehavior<LinearLayout>

    private var isDetailsSheetBehaviourVisible: Boolean by Delegates.observable(false) { prop, old, new ->

        Log.i("Home", " (line 104): isXtraaDescriptionVisible: $old -> $new")

        if (new) {

            showDetailsSheet()

        } else {

            hideDetailsSheet()

        }

    }

    fun showDetailsSheet() {
        detailsSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    fun hideDetailsSheet() {

        detailsSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

    }







    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        val menu = navView.getMenu()

        nav_button.setOnClickListener {
            drawer_layout.openDrawer(GravityCompat.START);
        }
        val header = nav_view.getHeaderView(0);
        header.setOnClickListener {
            startActivity(Intent(this, Profile::class.java))
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        val permissions = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
        if (!isLocationEnabled(this)) {
            showLocationIsDisabledAlert()
        }
        var hasPermission = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasPermission == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(applicationContext,"Searching nearby tournaments",Toast.LENGTH_SHORT).show()
        }
        else{
            Toast.makeText(applicationContext,"Enable location",Toast.LENGTH_SHORT).show()

            ActivityCompat.requestPermissions(this, permissions,0)

        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
        var locate = findViewById<ImageView>(R.id.imgMyLocation)
     detailsSheetBehavior = BottomSheetBehavior.from<LinearLayout>(bottom_sheets)
        isDetailsSheetBehaviourVisible = false
        setupPermissions()





        locate.setOnClickListener {
            if (!isLocationEnabled(this)) {
                showLocationIsDisabledAlert()
            }else{
            moveCameratoUser()
        }}

    locationListener
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


   getProfile()
    }

    fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i("Error", "Permission to call denied")
        }
    }
    private fun getProfile(){
        var accessToken= sharedPreference.getValue(this,"AccessToken")

        api.getProfile(accessToken)
            .subscribeOn(Schedulers.newThread())

            .observeOn(AndroidSchedulers.mainThread())

            .subscribeOn(Schedulers.io())

            .subscribe({

                Log.i("Profile", "loadProfile (line 39): $it")

                tmName.setText(it.teamName)
                if (it.photo==null)
                {profileImageView.setImageResource(R.drawable.ic_roma_1)

                } else {
                    imageProgressHeader.visibility = View.VISIBLE

                    Picasso.with(this).load(it.photo)
                        .placeholder(R.drawable.ic_roma_1)
                        .error(R.drawable.ic_roma_1)
                        .into(profileImageView, object : Callback {
                            override fun onSuccess() {
                                Log.i("Map", "onSuccess (line 244): ")
                                imageProgressHeader.visibility = View.GONE
                            }


                            override fun onError() {
                                Log.i("Map", "onError (line 251): ")
                                imageProgressHeader.visibility = View.GONE
                            }

                        })

                }

            }, {
                Log.e("UserViewModel", "loadUsers (line 260): ", it)

            })
    }

    private fun getTournaments() {
        val radius = DEFAULT_DISPLAY_RADIUS.toDouble()
        val latitude = userLocation.latitude
        val longitude = userLocation.longitude
        var accessToken= sharedPreference.getValue(this,"AccessToken")
        val location = com.shahinsha.tournamentfixture.pojo.Location(latitude, longitude)
        Log.i("Home", "getWhistlers (line 633): $location")
        val getTournamentReq = GetTournamentReq(location, radius)
        val userId = sharedPreference.getValue(this, "UserId")
        api.getTournaments(getTournamentReq,accessToken)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({

                Log.i(TAG, "searchresponse" + it)
                Fixtures = it.tournaments;

                Log.d(TAG, "check whistlers" + Fixtures)
                TournamentMap.clear()
                if (Fixtures.isEmpty()) {
                    Log.i("Home", "getWhistlers (line 644): No whistlers")

                    return@subscribe
                }else{

                for (Fixture in Fixtures) {
                    val markerLocation = LatLng(Fixture.location.coordinates[0], Fixture.location.coordinates[1])
                    if (Fixture._id != userId) {
                        marker = TournamentMap.addMarker(MarkerOptions()
                            .position(markerLocation)
                            .icon(
                                BitmapDescriptorFactory
                                    .fromResource(R.drawable.soccer_pin)))
                        marker.tag = Fixture._id
                    }}
                }

            }, {

                Toast.makeText(this,"Error",Toast.LENGTH_SHORT).show()
                Log.e(TAG, "getWhistlers error: " + it.message)

            })
    }

    override fun onMapReady(p0: GoogleMap) {



        p0.setOnMarkerClickListener(this)
        p0.uiSettings.isCompassEnabled = false
        p0.uiSettings.isMyLocationButtonEnabled = false
        TournamentMap = p0;
        TedPermission.with(this)
            .setPermissionListener(permissionListener)

            .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
            .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION)
            .check();


    }
    fun moveCameratoUser() {

        Log.d(TAG, "moveCameratoUser: ");

        try {

            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(userLocation, 18f);

            TournamentMap.animateCamera(cameraUpdate);

        } catch (ex: Exception) {

            Log.d(TAG, "moveCameratoUser: Location is null. cannot move to user location");

            ex.printStackTrace();

        }


    }



    override fun onMarkerClick(p0: Marker?): Boolean {
        Log.i("Map", "onMarkerClick (line 243): " + p0?.tag)
        if (p0?.tag != marker) {

            val selectedMarkerId = p0?.tag;
            val selectedTournament = Fixtures.find { w -> w._id == selectedMarkerId }

            Log.i("Home", "onMarkerClick (line 89): Selected whistler: $selectedTournament")

            if (selectedTournament != null) {
                selectedTournaments = selectedTournament
                selectedTournamentId = selectedTournament._id

                sharedPreference.save(this, "selectWhistlerId", selectedTournamentId)
                Log.i("Home", "onMarkerClick (line 57): $selectedTournament")

               showTournaments()
            } else {
                Log.i("Home", "onMarkerClick (line 99): Whistler lost.")
            }

        }


        return true

    }

    @SuppressLint("MissingPermission")
    private fun showTournaments(){


            clubname.text = selectedTournaments.name.capitalize()
            var dates = selectedTournaments.tournaments[0].date
        val parser =  SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val formatter = SimpleDateFormat("dd MMMM yyyy")
        val formattedDate = formatter.format(parser.parse(dates))
        date.text = formattedDate

            fee.text = selectedTournaments.tournaments[0].fee.toString()
           description.setMovementMethod(ScrollingMovementMethod())
            description.text = selectedTournaments.tournaments[0].description
            var phone = selectedTournaments.phone
            call.setOnClickListener {
                val permissions = arrayOf(android.Manifest.permission.CALL_PHONE)

                var hasPermission = ContextCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE);
                if (hasPermission == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(applicationContext,"Dialing...",Toast.LENGTH_SHORT).show()
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + phone)
                    startActivity(callIntent)
                }
                else if(hasPermission == PackageManager.PERMISSION_DENIED){
                    Toast.makeText(applicationContext,"Give permission to call",Toast.LENGTH_SHORT).show()

                    ActivityCompat.requestPermissions(this, permissions,0)

                }


            }
        cancel.setOnClickListener {
            isDetailsSheetBehaviourVisible = false
        }

            Log.i("Home", "showWhistlerDetails (line 200): ${selectedTournaments.photo}")

            if (selectedTournaments.photo == null) {
                dp.setImageResource(R.drawable.ic_roma_1)

            } else {
                bottomSheetProgressBar.visibility = View.VISIBLE

                Picasso.with(this).load(selectedTournaments.photo)
                    .placeholder(R.drawable.ic_roma_1)
                    .error(R.drawable.ic_roma_1)
                    .into(dp, object : Callback {
                        override fun onSuccess() {
                            Log.i("Home", "onSuccess (line 204): ")
                            bottomSheetProgressBar.visibility = View.GONE
                        }


                        override fun onError() {
                            Log.i("Home", "onError (line 209): ")
                            bottomSheetProgressBar.visibility = View.GONE
                        }

                    })

            }
        isDetailsSheetBehaviourVisible = true


    }

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
        latitude = location.latitude.toString()
            longitude = location.longitude.toString()
        }

    }

    private fun isLocationEnabled(mContext: Context): Boolean {
        val lm = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER)
    }
    private fun showLocationIsDisabledAlert() {
        alert("We can't show your position because you generally disabled the location service for your device.") {
            yesButton {
            }
            neutralPressed("Settings") {
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
        }.show()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

}



